import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from "@ngxs/store";
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';

// used to create fake backend
import { fakeBackendProvider } from '@domain/core/interceptors';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'modules/share/fuse/fuse-config';

import { AppRoutingModule } from './app-routing.module';
import { AlertComponent } from 'modules/share/alert/alert.component';
import { LayoutModule } from 'modules/share/layout/layout.module';
import { JwtInterceptor, ErrorInterceptor } from '@domain/core/interceptors';
import { AppComponent } from './app.component';
import { AuthState } from '@domain/store/state/auth.state';
import { environment } from 'environments/environment';
import { UserState } from '@domain/store/state/user.state';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgxsModule.forRoot([AuthState, UserState], { developmentMode: !environment.production }),
    NgxsStoragePluginModule.forRoot({
      key: AuthState
    }),
    HttpClientModule,

    AngularFireModule.initializeApp(environment.firebase),
    AngularFireMessagingModule,

    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: environment.production
    }),

    // Material moment date module
    MatMomentDateModule,

    // Material
    MatButtonModule,
    MatIconModule,

    // Fuse modules
    FuseModule.forRoot(fuseConfig),
    FuseProgressBarModule,
    FuseSharedModule,
    FuseSidebarModule,
    FuseThemeOptionsModule,

    AppRoutingModule,
    TranslateModule.forRoot(),
    LayoutModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
