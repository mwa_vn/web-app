import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@domain/core/security/auth.guard';
import { AdminGuard } from '@domain/core/security/admin.guard';
import { UserService } from '@domain/services/user.service';

const routes: Routes = [
  {
    path: 'advertise',
    loadChildren: () => import('./modules/advertise/advertise.module').then(mod => mod.AdvertiseModule),
   // canActivate: [AuthGuard, AdminGuard],
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    resolve: {
      data: UserService
    },
    children: [
      {
        path: '',
        loadChildren: () => import('modules/profile/profile.module').then(mod => mod.ProfileModule)
      },
      {
        path: ':userId',
        loadChildren: () => import('modules/profile/profile.module').then(mod => mod.ProfileModule)
      }
    ]
  },
  {
    path: 'advertise',
    loadChildren: () => import('./modules/advertise/advertise.module').then(mod => mod.AdvertiseModule),
    // canActivate: [AuthGuard, AdminGuard],
  },
  {
    path: 'home',
    loadChildren: () => import('modules/profile/profile.module').then(mod => mod.ProfileModule),
    canActivate: [AuthGuard],
    resolve: {
      data: UserService
    }
  },
  {
    path: 'admin',
    canActivate: [AuthGuard, AdminGuard],
    resolve: {
      data: UserService
    },
    children: [
      {
        path: 'post-management',
        loadChildren: () => import('modules/post-management/post-management.module').then(mod => mod.UnhealthyPostsModule)
      },
      {
        path: 'advertise',
        loadChildren: () => import('modules/advertise/advertise.module').then(mod => mod.AdvertiseModule)
      },
      {
        path: 'activate',
        loadChildren: () => import('modules/user-activate/user-activate.module').then(mod => mod.UserActivateModule)
      },
      {
        path: '**',
        redirectTo: 'post-management'
      }
    ]
  },
  {
    path: 'social',
    loadChildren: () => import('modules/social/social.module').then(mod => mod.SocialModule),
    canActivate: [AuthGuard],
    resolve: {
      data: UserService
    }
  },
  {
    path: 'users',
    loadChildren: () => import('modules/user/user.module').then(mod => mod.UserModule)
  },
  {
    path: 'errors',
    loadChildren: () => import('modules/errors/errors.module').then(mod => mod.ErrorsModule),
    resolve: {
      data: UserService
    }
  },
 
  // otherwise redirect to home
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
