import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable, throwError } from 'rxjs';
import { environment } from 'environments/environment';
import { AuthStateModel } from '@domain/store/model/auth.model';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    constructor(private http: HttpClient) {}

    login(username: string, password: string): Observable<AuthStateModel> {
      return this.http.post<any>(`${environment.apiUrl}/users/authenticate`, { username, password });
    }

    logout(): Observable<null> {
      return of(null);
    }
}
