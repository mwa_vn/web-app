import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class UnhealthyConfigService implements Resolve<any> {
  phrases: any[];
  onPhrasesChanged: BehaviorSubject<any>;

  constructor(
    private http: HttpClient
  ) {
    this.onPhrasesChanged = new BehaviorSubject({});
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getUnhealthyWords()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  // unhealthy config APIs
  getUnhealthyWords(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${environment.apiUrl}/profanities`).subscribe(
        (response: any) => {
          this.phrases = response;
          this.onPhrasesChanged.next(this.phrases);
          resolve(response);
        },
        reject
      );
    });
  }
  reloadUnhealthyWords(): Promise<any> {
    return this.getUnhealthyWords();
  }
  addUnhealthyPhrase(listwords, lang, enabled) {
    return new Promise((resolve, reject) => {
      this.http.post(`${environment.apiUrl}/profanities`, {
        language: lang, enabled,
        listwords
      }).subscribe(
        (response: any) => resolve(response),
        reject
      );
    });
  }
  saveUnhealthyPhrase(id, listwords, lang, enabled) {
    return new Promise((resolve, reject) => {
      this.http.put(`${environment.apiUrl}/profanities/${id}`, {
        language: lang, enabled,
        listwords
      }).subscribe(
        (response: any) => resolve(response),
        reject
      );
    });
  }
  deleteUnhealthyConfig(id) {
    //console.log(id);
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl}/profanities/${id}`)
        .subscribe(
          (response: any) => resolve(response),
          reject
        );
    });
  }
}
