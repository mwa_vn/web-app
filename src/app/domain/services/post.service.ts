import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class PostsService implements Resolve<any> {
  posts: any[];
  onPostsChanged: BehaviorSubject<any>;

  constructor(
    private http: HttpClient
  ) {
    this.onPostsChanged = new BehaviorSubject({});
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getUnhealthyPosts()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  // unhealthy config APIs
  getUnhealthyPosts(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${environment.apiUrl}/posts?unhealthy=true`).subscribe(
        (response: any) => {
          this.posts = response;
          this.onPostsChanged.next(this.posts);
          resolve(response);
        },
        reject
      );
    });
  }
  reloadPosts(): Promise<any> {
    return this.getUnhealthyPosts();
  }
  changeStatusOfPost(id, disabled): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(`${environment.apiUrl}/posts/${id}/changePublishStatus`, { disabled }).subscribe(
        (response: any) => resolve(response),
        reject
      )
    })
  }

  getPostDetail(id):Promise<any>{
    return null;
  }
}
