import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject, throwError } from 'rxjs';
import { mergeMapTo, tap, catchError } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { AuthState } from '@domain/store/state/auth.state';
import * as firebase from 'firebase/app';
import isEmpty from 'lodash/fp/isEmpty';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { keys } from 'lodash/fp';

@Injectable({ providedIn: 'root' })
export class NotificationService {
  hasPermission: boolean = false;
  onNotificationLoaded = new BehaviorSubject([]);
  onNotificationAdded = new BehaviorSubject(null);
  onNotificationUpdated = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
    private db: AngularFireDatabase,
    private messaging: AngularFireMessaging,
    private store: Store
  ) { }

  private updateToken(token) {
    const isAuthenticated = this.store.selectSnapshot(AuthState.isAuthenticated);
    const userId = this.store.selectSnapshot(AuthState.userId);
    if (isAuthenticated) {
      this.db.object(`fcmTokens/${userId}/notificationTokens`).update({ [token]: firebase.database.ServerValue.TIMESTAMP })
    }
    this.hasPermission = !!token;
  }

  getPermission() {
    return this.messaging.requestPermission
      .pipe(mergeMapTo(this.messaging.getToken))
      .pipe(
        tap(
          token => this.updateToken(token)
        ),
        catchError(error => {
          console.log('Unable to get permission to notify.', error);
          return throwError(error);
        })
      );
  }

  getNotifications() {
    const isAuthenticated = this.store.selectSnapshot(AuthState.isAuthenticated);
    const userId = this.store.selectSnapshot(AuthState.userId);
    if (isAuthenticated && this.hasPermission) {
      const notificationQuery = this.db.list(`notifications/${userId}`)
        .query
        .orderByChild('timestamp')
        .limitToLast(100);
      notificationQuery
        .on('child_added', snapshot => {
          const notification = snapshot.val();
          const id = snapshot.key;
          if(notification != null)
            this.onNotificationAdded.next({ id, ...notification });
        });
      notificationQuery
        .on('child_changed', snapshot => {
          const id = snapshot.key;
          const notification = snapshot.val();
          if (!isEmpty(notification)) {
            this.onNotificationUpdated.next({ id, ...notification });
          }
        })
      notificationQuery
        .on('value', snapshot => {
          let notifications = snapshot.val() || {};
          notifications = keys(notifications).filter(key => !isEmpty(key)).map(id => ({ id, ...notifications[id] }))
          this.onNotificationLoaded.next(notifications);
        });
    }
  }

  viewNotification(postId) {
    return this.http.get(`${environment.apiUrl}/notifications/view/${postId}`);
  }

  clearNotifications() {
    this.onNotificationLoaded.next([]);
  }
}