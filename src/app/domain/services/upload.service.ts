import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "environments/environment";

@Injectable({ providedIn: 'root' })
export class UploadService {
  SERVER_URL: string = environment.apiUrl;
  constructor(private http: HttpClient) { }

  upload(path, formData) {
    return this.http.post<any>(`${this.SERVER_URL}${path}`, formData, {
      reportProgress: true,  
      observe: 'events'
    });
  }
}
