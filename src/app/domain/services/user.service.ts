import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '@domain/models';
import { environment } from 'environments/environment';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { GetProfile } from '@domain/store/actions/user.action';
import { AuthState } from '@domain/store/state/auth.state';

@Injectable({ providedIn: 'root' })
export class UserService implements Resolve<any> {
  constructor(
    private http: HttpClient,
    private store: Store
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return new Promise((resolve, reject) => {
      const isAuthenticated = this.store.selectSnapshot(AuthState.isAuthenticated);
      if (isAuthenticated) {
        const userId = this.store.selectSnapshot(AuthState.userId);
        this.store.dispatch(new GetProfile(userId)).subscribe(
          () => resolve(true),
          reject
        );
      } else {
        resolve(false);
      }
    });
  }

  getAll() {
    return this.http.get<User[]>(`${environment.apiUrl}/users`);
  }

  register(user: User) {
    return this.http.post<any>(`${environment.apiUrl}/users/register`, user);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/users/${id}`);
  }

  getProfile(userId: string) {
    return this.http.get<User>(`${environment.apiUrl}/users/profile/${userId}`);
  }

  updateProfile(user: User){
    return this.http.put(`${environment.apiUrl}/users/${user.id}`, user);
  }

  submitActiveRequest(explaination: string) {
    return this.http.post(`${environment.apiUrl}/users/submitActiveRequest`, { explaination });
  }

  activate(userId, disabled) {
    return this.http.put(`${environment.apiUrl}/users/${userId}/activate`, { disabled: disabled });
  }

  getInActiveUsers() {
    return this.http.get(`${environment.apiUrl}/users/list/inactive`);
  }
}
