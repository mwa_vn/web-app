import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { User } from '@domain/models';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { BehaviorSubject, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SocialService {
  onFollowStatusChanged = new BehaviorSubject<boolean>(null);

  constructor(private http: HttpClient) {}

  fetchPeople(search:string = '', category = 'all', lastId: string = '') {
    return this.http.get<any>(`${environment.apiUrl}/users/people?search=${encodeURIComponent(search)}&category=${category}&lastId=${lastId}`);
  }

  fetchTop10People() {
    return this.http.get<any>(`${environment.apiUrl}/users/people`);
  }

  updateFollow(peopleId: string, isFollow: boolean) {
    return this.http.put(`${environment.apiUrl}/users/follow/${peopleId}/${isFollow}`, {}).pipe(
      tap(
        response => {
          this.onFollowStatusChanged.next(!isFollow);
          return response;
        }
      )
    );
  }
}
