import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "environments/environment";
import Post from "@domain/models/post.model";
import {Observable, BehaviorSubject} from "rxjs";

@Injectable({ providedIn: 'root' })
export class PostClientService {
  constructor(private http: HttpClient) { }

  public onSearchChanged = new BehaviorSubject('');
  get search() {
    return this.onSearchChanged.value;
  }
  set search(val: string) {
    this.onSearchChanged.next(val);
  }

  getAll() {
    return this.http.get<Post[]>(`${environment.apiUrl}/posts`);
  }

  getPostById(id: string){
    return this.http.get<Post>(`${environment.apiUrl}/posts/${id}`);
  }

  addPost(post) {
    return this.http.post(`${environment.apiUrl}/posts`, post);
  }

  getPostsByUser(userId: string, search = '', createdAt = ''): Observable<Post[]> {
    return this.http.get<Post[]>(`${environment.apiUrl}/posts/user/${userId}?search=${search}&createdAt=${createdAt}`)
  }

  getFeedsByUser(search = '', createdAt = '') {
    return this.http.get<Post[]>(`${environment.apiUrl}/posts/feeds?search=${search}&createdAt=${createdAt}`)
  }

  likePost(postId: string, likeUserId: string, liked: boolean) {
    return this.http.put<Post>(`${environment.apiUrl}/posts/${postId}/like`, { userId: likeUserId , like: liked})
  }

  addCommentToPost(postId: string, commentUserId: string, message: string) {
    return this.http.put<Post>(`${environment.apiUrl}/posts/${postId}/comments`, { userId: commentUserId , message: message})
  }

  likePostComment(commentId: string, likeUserId: string, liked: boolean, postId: string) {
    return this.http.put<Post>(`${environment.apiUrl}/posts/comments/${commentId}/like`, { postId: postId, userId: likeUserId , like: liked})
  }

  removeComment(commentId: string) {
    return this.http.delete(`${environment.apiUrl}/posts/comments/${commentId}`)
  }

}
