import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { AuthState } from '@domain/store/state/auth.state';

@Injectable({ providedIn: 'root' })
export class AdminGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const isAdmin = this.store.selectSnapshot(AuthState.isAdmin);
    if (!isAdmin) {
      // not logged in so redirect to login page with the return url
      this.router.navigate(['/errors/403']);
      return false;
    }
    return true;
  }
}
