import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Store } from '@ngxs/store';
import { AuthState } from '@domain/store/state/auth.state';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private store: Store) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const token = this.store.selectSnapshot(AuthState.token);
    const token_type = this.store.selectSnapshot(AuthState.tokenType) || 'Bearer';
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `${token_type} ${token}`
        }
      });
    }

    return next.handle(request);
  }
}
