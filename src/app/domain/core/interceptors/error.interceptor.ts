import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Store } from '@ngxs/store';
import { Logout } from '@domain/store/actions/auth.action';
import { Router } from '@angular/router';
import { HttpError } from '@domain/enums/http-error.enum';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private store: Store, private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(err => {
        if (err.status === HttpError.Unauthorized) {
          // auto logout if 401 response returned from api
          this.store.dispatch(new Logout)
            .subscribe(
              () => location.reload(true)
            );
        } else if (err.status === HttpError.Forbidden) {
          this.router.navigate(['/errors/403']);
          return next.handle(request);
        } else if (err.status === HttpError.NotFound) {
          this.router.navigate(['/errors/404']);
          return next.handle(request);
        }  else if (err.status >= HttpError.InternalServerError || err instanceof HttpErrorResponse) {
          this.router.navigate(['/errors/500']);
          return next.handle(request);
        }
        const error = err.error.message || err.statusText;
        return throwError(error);
      })
    )
  }
}
