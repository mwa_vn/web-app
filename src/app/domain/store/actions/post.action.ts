
import Post from "../../models/post.model";
import {ActionStateEnum} from "../enums/action-state.enum";

export class SetPost {
  static readonly type = '[POST] Set Post';
  constructor(public post: Post, public actionState: ActionStateEnum ){}
}


export class SavePost {
  static readonly type = '[POST] Save Post';
  constructor(public post: Post){}
}

export class ClearPost {
  static readonly type = '[POST] Clear Post';
}

export class GetPost {
  static readonly type = '[POST] Get Post';
}
