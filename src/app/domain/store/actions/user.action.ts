import { ActionStateEnum } from '../enums/action-state.enum';
import { User } from '@domain/models';

export class GetProfile {
  static readonly type = '[User] Get Profile';
  constructor(public userId: string) {}
}

export class SetProfile {
  static readonly type = '[UserProfile] Set UserProfile';
  constructor(public userProfile: User, public actionState: ActionStateEnum ){}
}

export class ClearProfile {
  static readonly type = '[UserProfile] Clear UserProfile';
}