export enum ActionStateEnum {
  SAVE,
  UPDATE,
  DELETE,
  CLEAR
}
