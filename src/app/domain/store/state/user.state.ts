import { State, Selector, Action, StateContext } from '@ngxs/store';
import { User } from "@domain/models";
import { UserService } from '@domain/services/user.service';
import { GetProfile, SetProfile, ClearProfile } from '../actions/user.action';
import { tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';

export interface UserModelState {
  profile: User;
};

@State<UserModelState>({
  name: 'currentUser',
  defaults: {
    profile: null
  }
})
@Injectable({ providedIn: 'root' })
export class UserState {
  constructor(private userService: UserService) {}

  @Selector()
  static profile(state: UserModelState) { return state.profile; }

  @Selector()
  static isLoaded(state: UserModelState) { return state.profile !== null; }

  @Action(GetProfile)
  getProfile(ctx: StateContext<UserModelState>, { userId }: GetProfile) {
    const state = ctx.getState();
    const isLoaded = UserState.isLoaded(state);
    if (isLoaded) {
      const userProfile = UserState.profile(state);
      if(userId === userProfile.id)
        return of(userProfile);
    }
    return this.userService.getProfile(userId).pipe(
      tap((data: User) => ctx.patchState({ profile: data }))
    );
  }

  @Action(SetProfile)
  setProfile(ctx: StateContext<UserModelState>, {userProfile}: SetProfile) {
    ctx.patchState({ profile: userProfile })
  }

  @Action(ClearProfile)
  clearProfile(ctx: StateContext<UserModelState>) {
    ctx.setState({
      profile: null
    });
  }
  
}
