import {Action, Selector, State, StateContext} from '@ngxs/store';
import {PostSlotModel} from "../model/post-slot.model";
import {ClearPost, SavePost, SetPost} from "../actions/post.action";
import cloneDeep from 'lodash/cloneDeep';
import {ActionStateEnum} from "@domain/store/enums/action-state.enum";
import {PostClientService} from "@domain/core/http/post-client.service";
import {Injectable} from "@angular/core";

type PoststateModel = PostSlotModel;

@State<PoststateModel>({
  name: 'currentWorkingPost',
  defaults: {}
})
@Injectable()
export class CurrentWorkingPostState {

  constructor(private postClientService: PostClientService) {
  }

  @Selector()
  static post(state: PoststateModel) {
    return cloneDeep(state.slot);
  }

  @Action(ClearPost)
  clearPost(ctx: StateContext<PoststateModel>) {
    ctx.setState({
      slot: {},
      metadata: {
        state: ActionStateEnum.CLEAR,
      }
    });
  }

  @Action(SavePost)
  savePost(ctx: StateContext<PoststateModel>, {post}: SavePost) {
    this.postClientService.addPost({
      message: post.message,
      userId: post.user.id,
      images: post.images,
      isNotified: post.isNotified,
    }).subscribe((posted) => {
      this.clearPost(ctx);
    });
  }

  @Action(SetPost)
  setPost(ctx: StateContext<PoststateModel>, {post, actionState}: SetPost) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      slot: post,
      metadata: {
        state: actionState,
      }
    });
  }
}
