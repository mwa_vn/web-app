import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Login, Logout } from '@domain/store/actions/auth.action'
import { AuthenticationService } from '@domain/services/authentication.service';
import { tap } from 'rxjs/operators';
import { AuthStateModel } from '../model/auth.model';
import { Injectable } from '@angular/core';
import { ClearProfile } from '../actions/user.action';

@State<AuthStateModel>({
  name: 'auth'
})
@Injectable()
export class AuthState {
  constructor(private authService: AuthenticationService) {}

  @Selector()
  static userId(state: AuthStateModel) { return state.id; }

  @Selector()
  static token(state: AuthStateModel) { return state.token; }

  @Selector()
  static tokenType(state: AuthStateModel) { return state.token_type; }

  @Selector()
  static isAuthenticated(state: AuthStateModel): boolean {
    return !!state.token;
  }
  @Selector()
  static isAdmin(state: AuthStateModel): boolean {
    return state.role === 'ADMIN';
  }

  @Action(Login)
  login({ patchState }: StateContext<AuthStateModel>, { payload: { username, password } }: Login) {
    return this.authService.login(username, password).pipe(tap((result) => {
      patchState({ username, ...result });
    }))
  }

  @Action(Logout)
  logout({ setState, dispatch }: StateContext<AuthStateModel>) {
    return this.authService.logout().pipe(tap(() => {
      setState({ id: null, token: null, token_type: null, username: null, role: null });
      dispatch(ClearProfile).subscribe();
    }));
  }
}