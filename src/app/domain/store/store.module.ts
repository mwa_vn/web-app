import {NgxsModule} from "@ngxs/store";
import {CurrentWorkingPostState} from "./state/current-working-post.state";
import {NgModule} from "@angular/core";
import { UserState } from './state/user.state';

@NgModule({
  imports: [
    NgxsModule.forFeature([
      CurrentWorkingPostState
    ]),
  ],
})
export class StoreModule {}
