export interface AuthStateModel {
  id: string,
  username: string;
  token: string;
  token_type: string;
  role: string;
}