import {ActionStateEnum} from "../enums/action-state.enum";

export interface MetaDataModel {
  state?: ActionStateEnum;
}
