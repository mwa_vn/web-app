export interface StoreSlot<T, N> {
  slot?: T;
  metadata?: N;
}
