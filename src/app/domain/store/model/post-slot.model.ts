import {StoreSlot} from "./store-slot.model";
import Post from "../../models/post.model";
import {MetaDataModel} from "./meta-data.model";

export type PostSlotModel = StoreSlot<Post, MetaDataModel>;
