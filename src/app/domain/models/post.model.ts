import User from "./user.model";
import {CommentType} from "../enums/comment-type.enum";
import Media from "./media.model";
import PostComment from "./post-comment.model";
import ShortUser from "./short-user.model";

export interface LikeUser {
  id: string;
  name: string;
}

export default interface Post {
  id?: string;
  message?: string;
  images?: string[];
  unhealthy?: boolean;
  isNotified?: boolean;
  user?: ShortUser;
  createdAt?: Date;
  updatedAt?: Date;
  likes?: LikeUser[];
  comments?: PostComment[];
}
