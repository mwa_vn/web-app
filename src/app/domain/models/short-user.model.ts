export default interface ShortUser {
  id?: string;
  name?: string;
  avatar?: string;
}
