export default interface Media {
  id?: number;
  type?: string;
  preview?: string;
}
