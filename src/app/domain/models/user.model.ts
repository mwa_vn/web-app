import UserProfile from "./user-profile.model";
import {UserRole} from "../enums/user-role.enum";

export default class User {
  id?: string;
  email?: string;
  password?: string;
  fullName?: string;
  zipcode: number;
  dob: Date;
  disabled: boolean;
  role?: UserRole;
  createdAt: Date;
  updateAt: Date;
  userProfile?: UserProfile;
  following?: string[];
}
