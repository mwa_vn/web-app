import User from "./user.model";

export default interface UserProfile {
  firstname?: string;
  lastname?: string;
  gender?: string;
  photo?: string;
  school?: string;
  work?: string,
  phone?: string,
  name?: string
}
