import User from "./user.model";
import ShortUser from "./short-user.model";
import {LikeUser} from "./post.model";

export default interface PostComment {
  id?: string;
  message?: string;
  user?: ShortUser;
  likes?: LikeUser[];
  createdAt?: Date;
  updatedAt?: Date;
}
