import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserActivateComponent} from './user-activate/user-activate.component';
import {UserService} from "@domain/services/user.service";
import {RouterModule} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";

const routes = [
  {
    path: '',
    component: UserActivateComponent,
    resolve: {
      data: UserService
    }
  },
];

@NgModule({
  declarations: [UserActivateComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class UserActivateModule {
}
