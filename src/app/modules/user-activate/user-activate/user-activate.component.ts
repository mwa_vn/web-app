import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UserService} from "@domain/services/user.service";
import {PagingBaseComponent} from "modules/share/components/paging-base.component";
import {User} from "@domain/models";

@Component({
  selector: 'app-user-activate',
  templateUrl: './user-activate.component.html',
  styleUrls: ['./user-activate.component.less']
})
export class  UserActivateComponent  extends PagingBaseComponent<User> implements OnInit, AfterViewInit {

  users;

  constructor(private _userService: UserService) {
    super(['name', 'gender','email', 'actionButton']);
  }

  ngOnInit(): void {
    this._userService.getInActiveUsers().subscribe(users => {
      this.users = users;
      super.ngOnInit(this.users);
    })
   }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  onActivateclick(item: User) {
    this._userService.activate(item.id, false).subscribe(() => {
      this._userService.getInActiveUsers().subscribe(users => {
        this.users = users;
        super.ngOnInit(this.users);
      })
    });
  }
}
