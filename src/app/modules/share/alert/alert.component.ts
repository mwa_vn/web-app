import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AlertService } from '@domain/services/alert.service';

const DEFAULT_DELAY = 3000;

@Component({
  selector: 'alert',
  templateUrl: './alert.component.html'
})
export class AlertComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  message: any;

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    this.subscription = this.alertService.getAlert()
      .subscribe(message => {
        switch (message && message.type) {
          case 'success':
            message.cssClass = 'message-box';
            setTimeout(() => this.alertService.clear(), message.delay || DEFAULT_DELAY);
            break;
          case 'error':
            message.cssClass = 'message-box error';
            setTimeout(() => this.alertService.clear(), message.delay || DEFAULT_DELAY);
            break;
        }

        this.message = message;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}