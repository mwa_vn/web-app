import { NgModule } from '@angular/core';
import { MatSidenavModule } from '@angular/material/sidenav';

import { FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { ChatPanelModule } from 'modules/share/layout/components/chat-panel/chat-panel.module';
import { ContentModule } from 'modules/share/layout/components/content/content.module';
import { FooterModule } from 'modules/share/layout/components/footer/footer.module';
import { NavbarModule } from 'modules/share/layout/components/navbar/navbar.module';
import { QuickPanelModule } from 'modules/share/layout/components/quick-panel/quick-panel.module';
import { ToolbarModule } from 'modules/share/layout/components/toolbar/toolbar.module';

import { HorizontalLayout1Component } from 'modules/share/layout/horizontal/layout-1/layout-1.component';
import { DeactivationFormComponent } from '../../components/deactivation-form/deactivation-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        HorizontalLayout1Component,
        DeactivationFormComponent
    ],
    imports     : [
        MatSidenavModule,
        MatFormFieldModule,
        MatInputModule,
        CommonModule,
        MatButtonModule,
        ReactiveFormsModule,

        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        ChatPanelModule,
        ContentModule,
        FooterModule,
        NavbarModule,
        QuickPanelModule,
        ToolbarModule
    ],
    exports     : [
        HorizontalLayout1Component,
        DeactivationFormComponent
    ]
})
export class HorizontalLayout1Module
{
}
