import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSidebarModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { ChatPanelModule } from 'modules/share/layout/components/chat-panel/chat-panel.module';
import { ContentModule } from 'modules/share/layout/components/content/content.module';
import { FooterModule } from 'modules/share/layout/components/footer/footer.module';
import { NavbarModule } from 'modules/share/layout/components/navbar/navbar.module';
import { QuickPanelModule } from 'modules/share/layout/components/quick-panel/quick-panel.module';
import { ToolbarModule } from 'modules/share/layout/components/toolbar/toolbar.module';

import { VerticalLayout2Component } from 'modules/share/layout/vertical/layout-2/layout-2.component';

@NgModule({
    declarations: [
        VerticalLayout2Component
    ],
    imports     : [
        RouterModule,

        FuseSharedModule,
        FuseSidebarModule,

        ChatPanelModule,
        ContentModule,
        FooterModule,
        NavbarModule,
        QuickPanelModule,
        ToolbarModule
    ],
    exports     : [
        VerticalLayout2Component
    ]
})
export class VerticalLayout2Module
{
}
