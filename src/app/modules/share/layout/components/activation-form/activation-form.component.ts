import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import get from 'lodash/fp/get';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { UserService } from '@domain/services/user.service';
import { AlertService } from '@domain/services/alert.service';

@Component({
  selector: 'activation-form',
  templateUrl: './activation-form.component.html'
})
export class ActivationFormComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<ActivationFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private matDialog: MatDialog,
    private alertService: AlertService,
    private userService: UserService
  ) { }
  ngOnInit(): void {
  }
  getTitleNotification() {
    return get('notification.title')(this.data) || '';
  }
  getNotificationContent() {
    return get('notification.body')(this.data) || '';
  }
  approve() {
    const confirmDialog = this.matDialog.open(FuseConfirmDialogComponent);
    confirmDialog.componentInstance.confirmMessage = 'Are you sure you want to activate this user?';
      confirmDialog.afterClosed().subscribe(
        result => {
          if (result) {
            const userId = get('id')(this.data);
            this.userService.activate(userId, false).subscribe(
              () => this.dialogRef.close(),
              error => this.alertService.error(error)
            );
          }
        },
        error => this.alertService.error(error)
      );
  }
  decline() {
    this.dialogRef.close();
  }
}
