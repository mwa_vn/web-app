import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { UserService } from '@domain/services/user.service';
import { AlertService } from '@domain/services/alert.service';

@Component({
  selector: 'deactivation-form',
  templateUrl: './deactivation-form.component.html'
})
export class DeactivationFormComponent {
  explaination = new FormControl('', [Validators.required]);

  constructor(
    private _userService: UserService,
    private _alertService: AlertService
  ) { }

  submit() {
    if (this.explaination.hasError('required')) {
      return;
    }

    this._userService.submitActiveRequest(this.explaination.value).subscribe(
      () => {
        this._alertService.success('Request is submitted to administration. Please give us some days to evaluate your request. Thanks for your cooperation.');
        this.explaination.setValue('');
        this.explaination.reset();
      }
    );
  }
}