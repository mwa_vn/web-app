import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NotificationService } from '@domain/services/notification.service';
import has from 'lodash/fp/has';
import forEach from 'lodash/fp/forEach';
import isEmpty from 'lodash/fp/isEmpty';
import get from 'lodash/fp/get';
import noop from 'lodash/fp/noop';
import { Store } from '@ngxs/store';
import { UserState } from '@domain/store/state/user.state';
import { AuthState } from '@domain/store/state/auth.state';
import { MatDialog } from '@angular/material/dialog';
import { ActivationFormComponent } from '../activation-form/activation-form.component';
import {Router} from '@angular/router';

@Component({
  selector: 'user-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NotificationComponent implements OnInit, OnDestroy {
  unread: number = 0;
  notifications = [];
  ids = {};
  constructor(
    private notificationService: NotificationService,
    private store: Store,
    private _matDialog: MatDialog,
    private router: Router,
  ) { }
  ngOnDestroy(): void {
  }
  ngOnInit(): void {
    this.notificationService.getPermission().subscribe(
      () => {
        this.notificationService.getNotifications();
      }
    );
    this.notificationService.onNotificationLoaded.subscribe(
      notifications => {
        forEach(notification => this.appendNotification(notification))(notifications);
      }
    );
    this.notificationService.onNotificationAdded.subscribe(
      notification => this.appendNotification(notification)
    );
    this.notificationService.onNotificationUpdated.subscribe(
      notification => this.updateNotification(notification)
    );
  }
  viewPost(post) {
    const profile = this.store.selectSnapshot(UserState.profile);
    const isAdmin = this.store.selectSnapshot(AuthState.isAdmin);
    if (profile) {
      if (get('disabled')(profile) && !isAdmin) {
        return;
      }
      if (get('unread')(post) > 0) {
        this.notificationService.viewNotification(post.id).subscribe(
          () => {
            this.updateNotification({ id: post.id, unread: 0 });
            this.viewNotification(post);
          }
        );
      } else {
        this.viewNotification(post);
      }
    }
  }
  // private
  private viewNotification(post) {
    const unhealthy = post.unhealthy;
    const activeRequest = post.activeRequest
    if (unhealthy) {
      // post detail but it is unhealthy.
    } else if (activeRequest) {
      this._matDialog
        .open(ActivationFormComponent, { width: '700px', height: '500px', data: post })
        .afterClosed()
        .subscribe(noop);
    } else {
      // post detail
      this.router.navigate(['profile/post/'+post.id]);
    }
  }
  isEmptyNotification() {
    return isEmpty(this.notifications);
  }
  getNotificationTitle(notification) {
    return get('notification.title')(notification) || '';
  }
  appendNotification(notification) {
    if (has('id')(notification)) {
      if (!this.ids[notification.id]) {
        this.ids[notification.id] = true;
        this.notifications.splice(0, 0, notification);
        this.unread = this.notifications.filter(item => item.unread > 0).length;
      }
    }
  }
  updateNotification(notification) {
    if (has('id')(notification)) {
      if (this.ids[notification.id]) {
        const $notification = this.notifications.find(noti => noti.id === notification.id);
        if (!isEmpty($notification)) {
          $notification.unread = notification.unread;
        }
        this.unread = this.notifications.filter(item => item.unread > 0).length;
      }
    }
  }
}