import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import find from 'lodash/find';

import { FuseConfigService } from '@fuse/services/config.service';

import { navigation } from 'app/navigation/navigation';
import { Store } from '@ngxs/store';
import { Logout } from '@domain/store/actions/auth.action';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthState } from '@domain/store/state/auth.state';
import { UserState } from '@domain/store/state/user.state';
import {User} from "@domain/models";
import get from 'lodash/fp/get';
import isEmpty from 'lodash/fp/isEmpty';
import { PostClientService } from '@domain/core/http/post-client.service';
import { NotificationService } from '@domain/services/notification.service';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ToolbarComponent implements OnInit, OnDestroy {
  horizontalNavbar: boolean;
  rightNavbar: boolean;
  hiddenNavbar: boolean;
  currentUser: User;
  isAdmin = false;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _fuseConfigService: FuseConfigService,
    private _store: Store,
    private _router: Router,
    private postClientService: PostClientService,
    private notificationService: NotificationService
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    // Subscribe to the config changes
    this._fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((settings) => {
        this.horizontalNavbar = settings.layout.navbar.position === 'top';
        this.rightNavbar = settings.layout.navbar.position === 'right';
        this.hiddenNavbar = settings.layout.navbar.hidden === true;

        this._store.select(UserState.profile).subscribe(user => {
          this.currentUser = user;
        });
      });
    this._store.select(AuthState.isAdmin).subscribe(
      result => this.isAdmin = result
    );
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  search(event): void {
    this.postClientService.search = event.target.value;
  }

  logout() {
    this._store.dispatch(Logout).subscribe(
      () => {
        this._router.navigate(['/users/login?retu'], { queryParams: { returnUrl: this._router.url }});
      },
      error => console.error(error),
      () => {
        this.notificationService.clearNotifications();
      }
    )
  }

  isAdminRoute() {
    const url = this._router.url;
    return url.startsWith('/admin');
  }

  getProfileName() {
    const first = get('userProfile.firstname')(this.currentUser);
    const last = get('userProfile.lastname')(this.currentUser);
    const name = get('name')(this.currentUser);
    if (isEmpty(first) && isEmpty(last)) return name;
    return `${first} ${last}`;
  }
}
