import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AlertService } from '@domain/services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { Login } from '@domain/store/actions/auth.action';
import { AuthState } from '@domain/store/state/auth.state';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  serverError = false;

  constructor(
    private fuseConfigService: FuseConfigService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store,
    private alertService: AlertService
  ) {
    // Configure the layout
    this.fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
    // get return url from route parameters or default to '/home'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }

  ngOnInit(): void {
    // redirect to home if already logged in
    if (this.store.selectSnapshot(AuthState.isAuthenticated)) {
      this.router.navigate(['/']);
    }
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.store.dispatch(new Login({ username: this.f.email.value, password: this.f.password.value }))
      .pipe(
        catchError(error => throwError(error))
      )
      .subscribe(
        (data) => {
          if(data.auth.error){
            this.serverError = true;
          }
          const isAdmin = this.store.selectSnapshot(AuthState.isAdmin);
          if (isAdmin) {
            this.router.navigate(['admin']);
          } else {
            this.router.navigate([this.returnUrl]);
          }
        },
        error => this.alertService.error(error),
        () => this.loading = false
      );
  }
}
