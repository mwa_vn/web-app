import { OnInit, Component, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subject, merge, Observable, BehaviorSubject, fromEvent } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { map, takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { DataSource } from '@angular/cdk/table';
import { UnhealthyConfigService } from '@domain/services/unhealthy-config.service';
import { MatDialog } from '@angular/material/dialog';
import { UnhealthyFormComponent } from './form/form.component';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { AlertService } from '@domain/services/alert.service';
import filter from 'lodash/fp/filter';
import get from 'lodash/fp/get';
import indexOf from 'lodash/fp/indexOf';
import any from 'lodash/fp/any';

@Component({
  selector     : 'unhealthy-config',
  templateUrl  : './unhealthy-config.component.html',
  styleUrls    : ['./unhealthy-config.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class UnhealthyConfigComponent implements OnInit {
  dataSource: FilesDataSource | null;
  displayedColumns = ['_id', 'language', 'words', 'enabled'];

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild('filter', { static: true })
  filter: ElementRef;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _unhealthyConfigService: UnhealthyConfigService,
    private _matDialog: MatDialog,
    private _alertService: AlertService
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }
  ngOnInit(): void {
    this.dataSource = new FilesDataSource(this._unhealthyConfigService, this.paginator);

    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
  add() {
    this._matDialog
    .open(UnhealthyFormComponent, { width: '400px', data: { words: [] } })
    .afterClosed()
    .subscribe(
      () => {
        this._unhealthyConfigService.reloadUnhealthyWords();
        this._alertService.success('Adding successful!');
      }
    );
  }
  edit(post) {
    this._matDialog
    .open(UnhealthyFormComponent, { width: '400px', data: post })
    .afterClosed()
    .subscribe(
      () => {
        this._unhealthyConfigService.reloadUnhealthyWords();
        this._alertService.success('Saving successful!');
      }
    );
  }
  delete(id) {
    const confirmDialog = this._matDialog.open(FuseConfirmDialogComponent);
    confirmDialog.componentInstance.confirmMessage = 'Are you sure you want to delete?';
    confirmDialog.afterClosed().subscribe(
      result => {
        if (result) {
          this._unhealthyConfigService.deleteUnhealthyConfig(id)
            .then(() => {
              this._unhealthyConfigService.reloadUnhealthyWords();
              this._alertService.success('Deleting successful!');
            })
            .catch(error => this._alertService.error(error));
        }
      }
    )
  }
}

export class FilesDataSource extends DataSource<any>
{
  private _filterChange = new BehaviorSubject('');
  private _filteredDataChange = new BehaviorSubject('');

  constructor(
    private _unhealthyConfigService: UnhealthyConfigService,
    private _matPaginator: MatPaginator,
  ) {
    super();

    this.filteredData = this._unhealthyConfigService.phrases;
    //console.log(this._unhealthyConfigService.phrases);
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this._unhealthyConfigService.onPhrasesChanged,
      this._matPaginator.page,
      this._filterChange
    ];

    return merge(...displayDataChanges)
      .pipe(
        map(() => {
          let data = this._unhealthyConfigService.phrases.slice();

          data = this.filterData(data);

          this.filteredData = [...data];

          // Grab the page's slice of data.
          const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
          return data.splice(startIndex, this._matPaginator.pageSize);
        })
      );
  }

  get filteredData(): any {
    return this._filteredDataChange.value;
  }

  set filteredData(value: any) {
    this._filteredDataChange.next(value);
  }

  // Filter
  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filterData(data): any {
    if (!this.filter) {
      return data;
    }
    return filter(item => {
      const words = get('words')(item);
      // return any((w: string) => indexOf(this.filter)(w) >= 0)(words); // it doesn't work
      return any((w: string) => w.indexOf(this.filter) >= 0)(words);
    })(data);
  }

  disconnect(): void {
  }
}