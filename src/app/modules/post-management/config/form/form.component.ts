import { Component, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { UnhealthyConfigService } from '@domain/services/unhealthy-config.service';
import { AlertService } from '@domain/services/alert.service';
import get from 'lodash/fp/get';
import join from 'lodash/fp/join';
import isEmpty from 'lodash/fp/isEmpty';

@Component({
  selector: 'unhealthy-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UnhealthyFormComponent implements OnInit {
  phrase = new FormControl('', [Validators.required]);
  language = new FormControl('', [Validators.required]);
  enabled = new FormControl();

  constructor(
    private dialogRef: MatDialogRef<UnhealthyFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private unhealthyConfigService: UnhealthyConfigService,
    private alertService: AlertService
  ) {
  }
  ngOnInit(): void {
    this.phrase.setValue(join(', ')(get('words')(this.data)));
    this.language.setValue((get('language')(this.data)));
    this.enabled.setValue((get('enabled')(this.data)));
  }
  ok() {
    if (this.phrase.hasError('required')) {
      return;
    }
    const id = get('_id')(this.data);
    const savePromise = !isEmpty(id)
    ? this.unhealthyConfigService.saveUnhealthyPhrase(id, this.phrase.value, this.language.value, this.enabled.value)
    : this.unhealthyConfigService.addUnhealthyPhrase(this.phrase.value, this.language.value, this.enabled.value);
    savePromise
      .then(() => this.dialogRef.close(true))
      .catch(error => this.alertService.error(error));
  }
  cancel() {
    this.dialogRef.close(false);
  }
}
