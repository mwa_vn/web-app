import {NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule, FuseConfirmDialogModule } from '@fuse/components';
import { UnhealthyFormModule } from './config/form/form.module';
import { PostManagementComponent } from './post-management.component';
import { UnhealthyConfigComponent } from './config/unhealthy-config.component';
import { PostsService } from '@domain/services/post.service';
import { UnhealthyConfigService } from '@domain/services/unhealthy-config.service';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';

const routes = [
  {
    path: '',
    component: PostManagementComponent,
    resolve  : {
      data: PostsService
    }
  },
  {
    path: 'config',
    component: UnhealthyConfigComponent,
    resolve  : {
      data: UnhealthyConfigService
    }
  }
];
@NgModule({
  declarations: [
    PostManagementComponent,
    UnhealthyConfigComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatDialogModule,
    MatTooltipModule,
    UnhealthyFormModule,
    
    FuseSharedModule,
    FuseWidgetModule,
    FuseConfirmDialogModule
  ],
  providers: [
    PostsService
  ]
})
export class UnhealthyPostsModule {
}