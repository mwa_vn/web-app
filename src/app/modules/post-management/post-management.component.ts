import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { PostsService } from '@domain/services/post.service';
import { takeUntil } from 'rxjs/internal/operators';
import get from 'lodash/fp/get';
import first from 'lodash/fp/first';
import filter from 'lodash/fp/filter';
import moment from 'moment';
import { AlertService } from '@domain/services/alert.service';
import { MatDialog } from '@angular/material/dialog';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'post-management',
  templateUrl: './post-management.component.html',
  styleUrls: ['./post-management.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class PostManagementComponent implements OnInit {
  dataSource: FilesDataSource | null;
  displayedColumns = ['images', 'user', 'content', 'createdAt', 'unhealthy', 'disabled'];

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  @ViewChild('filter', { static: true })
  filter: ElementRef;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _unhealthyPostService: PostsService,
    private _alertService: AlertService,
    private _matDialog: MatDialog
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.dataSource = new FilesDataSource(this._unhealthyPostService, this.paginator, this.sort);

    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
  enable(post) {
    if (post) {
      const confirmDialog = this._matDialog.open(FuseConfirmDialogComponent);
      confirmDialog.componentInstance.confirmMessage = 'Are you sure you want to enable?';
      confirmDialog.afterClosed().subscribe(
        result => {
          if (result) {
            post.disabled = false;
            this._unhealthyPostService.changeStatusOfPost(get('id')(post), false)
              .then(() => {
                this._unhealthyPostService.reloadPosts();
                this._alertService.success('Post updated!');
              })
              .catch(error => this._alertService.error(error));
          }
        }
      );
    }
  }
  disable(post) {
    if (post) {
      const confirmDialog = this._matDialog.open(FuseConfirmDialogComponent);
      confirmDialog.componentInstance.confirmMessage = 'Are you sure you want to disable?';
      confirmDialog.afterClosed().subscribe(
        result => {
          if (result) {
            post.disabled = true;
            this._unhealthyPostService.changeStatusOfPost(get('id')(post), true)
              .then(() => {
                this._unhealthyPostService.reloadPosts();
                this._alertService.success('Post updated!');
              })
              .catch(error => this._alertService.error(error));
          }
        }
      );
    }
  }
  getPostContent(post) {
    return get('message')(post);
  }
  getPostImage(post) {
    const imageUrl = first(get('images')(post));
    return imageUrl || 'assets/images/ecommerce/product-image-placeholder.png';
  }
  getUserName(post) {
    return get('user.name')(post);
  }
  getPostCreated(post) {
    return moment(get('createdAt')(post)).format('MMM DD YYYY HH:mm');
  }
}

export class FilesDataSource extends DataSource<any>
{
  private _filterChange = new BehaviorSubject('');
  private _filteredDataChange = new BehaviorSubject('');

  constructor(
    private _postService: PostsService,
    private _matPaginator: MatPaginator,
    private _matSort: MatSort
  ) {
    super();

    this.filteredData = this._postService.posts;
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this._postService.onPostsChanged,
      this._matPaginator.page,
      this._filterChange,
      this._matSort.sortChange
    ];

    return merge(...displayDataChanges)
      .pipe(
        map(() => {
          let data = this._postService.posts.slice();

          data = this.filterData(data);

          this.filteredData = [...data];

          data = this.sortData(data);

          // Grab the page's slice of data.
          const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
          return data.splice(startIndex, this._matPaginator.pageSize);
        })
      );
  }

  // Filtered data
  get filteredData(): any {
    return this._filteredDataChange.value;
  }

  set filteredData(value: any) {
    this._filteredDataChange.next(value);
  }

  // Filter
  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filterData(data): any {
    if (!this.filter) {
      return data;
    }
    return filter(item => {
      const user = get('user.name')(item) || '';
      const post = get('message')(item) || '';
      const unhealthy = get('unhealthy')(item) || '';
      return user.indexOf(this.filter) >= 0 || post.indexOf(this.filter) >= 0 || unhealthy === true;
    })(data);
  }

  sortData(data): any[] {
    if (!this._matSort.active || this._matSort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._matSort.active) {
        case 'user':
          [propertyA, propertyB] = [a.user.name, b.user.name];
          break;
        case 'content':
          [propertyA, propertyB] = [a.message, b.message];
          break;
        case 'createdAt':
          [propertyA, propertyB] = [a.createdAt, b.createdAt];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
    });
  }

  disconnect(): void {
  }
}
