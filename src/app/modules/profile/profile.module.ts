import {NgModule} from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {MatTabsModule} from '@angular/material/tabs';

import {FuseSharedModule} from '@fuse/shared.module';
import {ProfileComponent} from "./profile.component";
import {ProfileTimelineComponent} from "./tabs/timeline/timeline.component";
import {ProfileAboutComponent} from "./tabs/about/about.component";
import {ProfilePhotosVideosComponent} from "./tabs/photos-videos/photos-videos.component";
import {CommentViewComponent} from './components/comment-view/comment-view.component';
import {PostAddComponent} from './components/post-add/post-add.component';
import {EditProfileComponent} from './components/edit-profile/edit-profile.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {MatMenuModule} from "@angular/material/menu";
import {MatRippleModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {MatRadioModule} from "@angular/material/radio";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {StoreModule} from "../../domain/store/store.module";
import { CommentEditComponent } from './components/comment-edit/comment-edit.component';
import { ProfilePictureComponent } from './components/profile-picture/profile-picture.component';
import { SidebarPeopleComponent } from 'modules/social/sidebar-people/sidebar-people.component';
import {MatTooltipModule} from "@angular/material/tooltip";
import { AdvertiseViewComponent } from './components/advertise-view/advertise-view.component';
import { PostDetailComponent } from './components/post-detail/post-detail.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';


const routes = [
  {
    path: '',
    component: ProfileComponent,
  },
  {
    path: 'post/:id',
    component: PostDetailComponent,
  }
];

@NgModule({
  declarations: [
    ProfileComponent,
    ProfileTimelineComponent,
    ProfileAboutComponent,
    ProfilePhotosVideosComponent,
    CommentViewComponent,
    PostAddComponent,
    EditProfileComponent,
    CommentEditComponent,
    ProfilePictureComponent,
    SidebarPeopleComponent,
    AdvertiseViewComponent,
    PostDetailComponent
  ],
  imports: [
    HttpClientModule,
    RouterModule.forChild(routes),

    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatTabsModule,
    MatToolbarModule,
    FuseSharedModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatDialogModule,
    MatInputModule,
    MatMenuModule,
    MatRippleModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule,
    MatProgressBarModule,

    StoreModule,
    MatTooltipModule,
  ]
})
export class ProfileModule {
}
