import {Component, ElementRef, Input, NgZone, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
import Post from "../../../../domain/models/post.model";
import User from "../../../../domain/models/user.model";
import {PostAddComponent} from "../../components/post-add/post-add.component";
import {MatDialog} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import PostComment from "@domain/models/post-comment.model";
import {UserState} from "@domain/store/state/user.state";
import {CurrentWorkingPostState} from "@domain/store/state/current-working-post.state";
import {ClearPost, SavePost} from "@domain/store/actions/post.action";
import {PostClientService} from "@domain/core/http/post-client.service";
import {BehaviorSubject} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import isEmpty from 'lodash/fp/isEmpty';
import get from 'lodash/fp/get';
import last from 'lodash/fp/last';
import concat from 'lodash/fp/concat';
import map from 'lodash/fp/map';
import join from 'lodash/fp/join';
import { SocialService } from '@domain/services/social.service';

@Component({
  selector: 'profile-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ProfileTimelineComponent implements OnInit, OnDestroy {
  message: string;
  currentUser: User;
  dialogRef: any;
  currentPost: Post;
  @Input() isTimeline = true;
  posts: Post[] = Array.from({length: 0});
  subject = new BehaviorSubject([]);
  profileUserId: string;
  @ViewChild('postAnchor', {read: ElementRef}) postAnchor: ElementRef;
  loadingData = false;

  /**
   * Constructor
   *
   * @param {ProfileService} _profileService
   */
  constructor(public _matDialog: MatDialog,
              private _store: Store,
              private _postClientService: PostClientService,
              private _socialService: SocialService,
              private route: ActivatedRoute,
              private ngZone: NgZone
  ) {
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.scroll, true);
  }

  ngOnInit(): void {
    this.profileUserId = this.route.snapshot.params.userId;
    this.route.params.subscribe(
      params => {
        this.profileUserId = params.userId || get('id')(this.currentUser);
        this.posts.length = 0;
        this.loadPost();
      }
    );
    this._store.select(UserState.profile).subscribe(user => {
      this.currentUser = user;
      this.loadPost();
      this.ngZone.runOutsideAngular(() => {
        window.addEventListener('scroll', this.scroll, true);
      })
    });
    this._store.select(CurrentWorkingPostState.post).subscribe(post => {
      this.currentPost = post;
      if (this.currentPost) {
        this.message = this.currentPost.message;
      }
    });
    this.subject.asObservable().subscribe(posts => {
      this.posts = posts;
    });
    this._postClientService.onSearchChanged.subscribe(
      search => {
        this.posts.length = 0;
        this.loadPost(this._postClientService.search);
      }
    );
    this._socialService.onFollowStatusChanged.subscribe(
      isFollow => {
        this.posts.length = 0;
        this.loadPost();
      }
    );
  }

  scroll = (ev) => {
    if ((ev.srcElement.scrollTop + ev.srcElement.offsetHeight) >= this.postAnchor.nativeElement.offsetTop) {
      if (!this.loadingData) {
        this.ngZone.run(() => this.loadPost(this._postClientService.search));
      }
      ev.stopPropagation();
      ev.preventDefault();
    }
  };

  loadPost(search = '') {
    if (this.loadingData || !this.currentUser) return;
    this.loadingData = true;
    const createdAt = get('createdAt')(last(this.posts)) || '';
    const userId = this.profileUserId || get('_id')(this.currentUser);
    if (this.isTimeline) {
      this._postClientService.getPostsByUser(userId, search, createdAt).subscribe((posts) => {
        this.subject.next(concat(this.posts,posts));
        this.loadingData = false;
      });
    } else {
      this._postClientService.getFeedsByUser(search, createdAt).subscribe((posts) => {
        this.subject.next(concat(this.posts,posts));
        this.loadingData = false;
      });
    }
  }

  addPost(data): void {
    const posted: Post = {
      ...data,
      message: this.message,
    };
    this._postClientService.addPost({
      message: this.message,
      userId: this.currentUser.id,
      images: posted.images,
      isNotified: posted.isNotified,
    }).subscribe((posted) => {
      this._store.dispatch(new ClearPost());
      this.subject.next(concat( [posted],this.posts));
    });
  }

  openPostDialog(): void {
    this.dialogRef = this._matDialog.open(PostAddComponent, {
      panelClass: 'app-post-add',
      autoFocus: true,
      minWidth: 1000,
      disableClose: true,
      data: {
        message: this.message,
      }
    });
    this.dialogRef.afterClosed()
      .subscribe(response => {
        if (!response) {
          return;
        }
        const actionType: string = response[0];
        switch (actionType) {
          /**
           * Send
           */
          case 'post':
            this.addPost(this.currentPost);
            break;
          /**
           * Delete
           */
          case 'delete':
            console.log('delete Mail');
            break;
        }
      });
  }

  onAddComment(comment: PostComment, post: Post) {
    this._postClientService.addCommentToPost(post.id, this.currentUser.id, comment.message).subscribe(updatedpost => {
      this.subject.next(this.posts.map( oldPost => {
        return oldPost.id === updatedpost.id? updatedpost: oldPost;
      }));
    });
  }

  isLiked(post: Post): boolean {
    return post.likes.find(user => user.id === this.currentUser.id) !== undefined;
  }

  likeorUnLikePost(post: Post) {
    const isLiked = this.isLiked(post);
    this._postClientService.likePost(post.id, this.currentUser.id, !isLiked).subscribe(updatedPost => {
      this.subject.next(this.posts.map( oldPost => {
        return oldPost.id === updatedPost.id? updatedPost: oldPost;
      }));
    });
  }

  isValidMessage() {
    return isEmpty(this.message);
  }

  likeComment(posted: Post) {
    this.subject.next(this.posts.map( oldPost => {
      return oldPost.id === posted.id? posted: oldPost;
    }));
  }

  getUsersLiked(likes) {
    return join(' \n ')(map('name')(likes));
  }
}
