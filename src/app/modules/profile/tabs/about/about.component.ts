import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { Store } from "@ngxs/store";
import { fuseAnimations } from '@fuse/animations';
//import {ProfileFakeDb} from "../../profile.mock";
import {UserState} from "@domain/store/state/user.state";
import {User} from "@domain/models";

@Component({
    selector     : 'profile-about',
    templateUrl  : './about.component.html',
    styleUrls    : ['./about.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileAboutComponent implements OnInit, OnDestroy
{
    about: any;
    user: User;
    // private _unsubscribeAll: Subject<any>;

    constructor(private _store: Store,)
    {
        // Set the private defaults
        // this._unsubscribeAll = new Subject();
        // this.postForm = this.createPostForm();
        this._store.select(UserState.profile).subscribe( user => {
        this.user = user;
    });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
      // TODO Intergrate with real data
      //this.about = ProfileFakeDb.about;
      
        // this._profileService.aboutOnChanged
        //     .pipe(takeUntil(this._unsubscribeAll))
        //     .subscribe(about => {
        //         this.about = about;
        //     });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        // this._unsubscribeAll.next();
        // this._unsubscribeAll.complete();
    }
}
