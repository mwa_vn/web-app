import {Component, Input, OnInit, NgZone} from '@angular/core';
import Post from "../../../../domain/models/post.model";
import User from "../../../../domain/models/user.model";
import {MatDialog} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import PostComment from "@domain/models/post-comment.model";
import {UserState} from "@domain/store/state/user.state";
import {SavePost} from "@domain/store/actions/post.action";
import {PostClientService} from "@domain/core/http/post-client.service";
import {BehaviorSubject} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import isEmpty from 'lodash/fp/isEmpty';
import join from 'lodash/fp/join';
import map from 'lodash/fp/map';


@Component({
  selector: 'post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit{
  message: string;
  currentUser: User;
  dialogRef: any;
  currentPost: Post;
  @Input() isTimeline = true;
  post: Post;
  subject = new BehaviorSubject({});
  profileUserId: string;
  postId: string;
  loadingData = false;

  /**
   * Constructor
   *
   * @param {ProfileService} _profileService
   */
  constructor(public _matDialog: MatDialog,
              private _store: Store,
              private _postClientService: PostClientService,
              private route: ActivatedRoute,
              private ngZone: NgZone
  ) {}
  
  ngOnInit(): void {
    this.profileUserId = this.route.snapshot.params.userId;
    this._store.select(UserState.profile).subscribe(user => {
      this.currentUser = user;
    });

    this.route.params.subscribe(
      params => {
        this.postId = params.id;
        this.getPost();
        
      });
  }

  getPost(){
    this.loadingData = true;
    this._postClientService.getPostById(this.postId).subscribe((postData) => {
      this.subject.next(postData);
    });
    this.subject.asObservable().subscribe(postData => {
      this.post = postData;
    });
  }

  postComment(data): void {
    const posted: Post = {
      ...data,
      message: this.message,
    };
    this._store.dispatch(new SavePost(posted)).subscribe( () => {
      this.message = '';
      setTimeout(() => {this.getPost()}, 100);
    });
  }

  onAddComment(comment: PostComment, post: Post) {
    this._postClientService.addCommentToPost(post.id, this.currentUser.id, comment.message).subscribe( postComment => {
      setTimeout(() => {this.getPost()}, 100);
    });
  }

  isLiked(post: Post): boolean {
    return post.likes.find(user => user.id === this.currentUser.id) !== undefined;
  }

  likeorUnLikePost(post: Post) {
    const isLiked = this.isLiked(post);
    this._postClientService.likePost(post.id, this.currentUser.id, !isLiked).subscribe( () => {
      setTimeout(() => {this.getPost()}, 100);
    });
  }

  isValidMessage() {
    return isEmpty(this.message);
  }
  
  likeComment(posted: Post) {
    const nxtPost = this.post.id === posted.id? posted: this.post;
    this.subject.next(nxtPost);
    this.getPost();
  }

  getUsersLiked(likes) {
    return join(' \n ')(map('name')(likes));
  }
}
