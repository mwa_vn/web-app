import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import PostComment from "@domain/models/post-comment.model";
import {Store} from "@ngxs/store";
import {UserState} from "@domain/store/state/user.state";
import {User} from "@domain/models";

@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.scss']
})
export class CommentEditComponent implements OnInit {

  message: string;
  currentUser: User;
  @Output() addCommentClick: EventEmitter<PostComment> = new EventEmitter<PostComment>();

  constructor(private _store: Store,) {
  }

  ngOnInit(): void {
    this._store.select(UserState.profile).subscribe(user => {
      this.currentUser = user;
    })
  }

  addComment() {
    const comment: PostComment = {
      message: this.message,
      user: this.currentUser,
      likes: [],
    };
    this.message = '';
    this.addCommentClick.emit(comment);
  }
}
