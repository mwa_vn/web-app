import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormControl, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import UserProfile from "@domain/models/user-profile.model";
import {SetProfile} from "@domain/store/actions/user.action";
import {ActionStateEnum} from "@domain/store/enums/action-state.enum";
import {UserState} from "@domain/store/state/user.state";
import {User} from "@domain/models";
import { UserService } from '@domain/services/user.service';

@Component({
  selector: 'edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  profileForm: FormGroup;
  currentUser: User;
  userProfile: UserProfile;
  @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;
  files = [];
  filesPathSaved = [];
  minDate: Date;
  maxDate: Date;

  constructor(public matDialogRef: MatDialogRef<EditProfileComponent>,
              @Inject(MAT_DIALOG_DATA) public _data: any,
              private _store: Store,
              private _userService: UserService
  ) {
    this.setBirthdayRange();
    this.profileForm = this.createProfileForm();
    this._store.select(UserState.profile).subscribe( user => {
      this.currentUser = user;
    });
    
  }

  private setBirthdayRange(){
    // Set the minimum to January 1st 20 years in the past and December 31st a year in the future.
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date();
  }

  createProfileForm(): FormGroup {
    return new FormGroup({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      dob: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      school: new FormControl(''),
      phone: new FormControl(''),
      work: new FormControl(''),
      zipcode: new FormControl('', Validators.required)
    });

    // this.registerForm = this.formBuilder.group({
    //   firstname: ['', Validators.required],
    //   lastname: ['', Validators.required],
    //   dob: ['', Validators.required],
    //   email: ['', [Validators.required, Validators.email]],
    //   password: ['', Validators.required],
    //   passwordConfirm: ['', [Validators.required, confirmPasswordValidator]],
    //   gender: ['', Validators.required],
    //   zipcode: ['', Validators.required]
    // });
  }

  ngOnInit(): void {
      if (this.currentUser.userProfile) {
        this.profileForm.get('firstname').setValue(this.currentUser.userProfile.firstname);
        this.profileForm.get('lastname').setValue(this.currentUser.userProfile.lastname);
        this.profileForm.get('dob').setValue(this.currentUser.dob)
        this.profileForm.get('gender').setValue(this.currentUser.userProfile.gender);
        this.profileForm.get('school').setValue(this.currentUser.userProfile.school);
        this.profileForm.get('work').setValue(this.currentUser.userProfile.work);
        this.profileForm.get('phone').setValue(this.currentUser.userProfile.phone);
        this.profileForm.get('zipcode').setValue(this.currentUser.zipcode);
      }

      this.matDialogRef.beforeClosed().subscribe(() => {
        this._store.dispatch(new SetProfile(this.currentUser, ActionStateEnum.UPDATE));
      });
  }

  createProfileData(): User {
    this.currentUser = {
      ...this.currentUser,
      dob: this.profileForm.get('dob').value,
      zipcode: this.profileForm.get('zipcode').value,
      userProfile: {
        ...this.currentUser.userProfile,
        lastname: this.profileForm.get('lastname').value,
        firstname: this.profileForm.get('firstname').value,
        gender: this.profileForm.get('gender').value,
        school: this.profileForm.get('school').value,
        work: this.profileForm.get('work').value,
        phone: this.profileForm.get('phone').value
      }
    }
    return this.currentUser;
  }

  saveProfile(): any {
    this._userService.updateProfile(this.createProfileData()).subscribe((result) => {
      this.matDialogRef.close();
    });
  }
}
