import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import PostComment from "@domain/models/post-comment.model";
import {Store} from "@ngxs/store";
import {UserState} from "@domain/store/state/user.state";
import User from "@domain/models/user.model";
import {PostClientService} from "@domain/core/http/post-client.service";
import Post from "@domain/models/post.model";
import {Router} from "@angular/router";
import { join, map } from 'lodash/fp';

@Component({
  selector: 'app-comment-view',
  templateUrl: './comment-view.component.html',
  styleUrls: ['./comment-view.component.less']
})
export class CommentViewComponent implements OnInit {

  @Input() comment: PostComment;
  @Input() post: Post;
  @Output() commentChanged: EventEmitter<Post> = new EventEmitter<Post>();
  currentUser: User;

  constructor( private _store: Store,
               private _postClientService: PostClientService,
               private router: Router,) { }

  ngOnInit(): void {
    this._store.select(UserState.profile).subscribe(user => {
      this.currentUser = user;
    });
  }

  likeorUnLike(comment: PostComment) {
    const isLiked = this.isLiked(comment);
    this._postClientService.likePostComment(comment.id, this.currentUser.id, !isLiked, this.post.id).subscribe( (post) => {
      this.commentChanged.emit(post);
    });
  }

  isLiked(comment: PostComment) {
    return comment.likes.find(user => user.id === this.currentUser.id) !== undefined;
  }

  removeComment() {
    this._postClientService.removeComment(this.comment.id).subscribe( (post) => {
      this.commentChanged.emit(post);
    });
  }

  canDelete(): boolean {
    return (this.post.user.id === this.currentUser.id || this.comment.user.id === this.currentUser.id );
  }

  navigateProfile() {
      //TODO: implement nevigate to profile of user
  }

  getUsersLiked(likes) {
    return join(' \n ')(map('name')(likes));
  }
}
