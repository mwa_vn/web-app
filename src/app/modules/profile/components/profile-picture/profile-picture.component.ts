import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import{ HttpClient,HttpErrorResponse, HttpEventType } from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UploadService} from '@domain/services/upload.service';
import {catchError, map} from 'rxjs/operators';
import {of} from 'rxjs';
import { UserService } from '@domain/services/user.service';
import {UserState} from "@domain/store/state/user.state";
import {User} from "@domain/models";
import { Store } from '@ngxs/store';
import {SetProfile} from "@domain/store/actions/user.action";
import { ActionStateEnum } from '@domain/store/enums/action-state.enum';
import get from 'lodash/fp/get';
import moment from 'moment';

@Component({
  selector: 'app-profile-picture',
  templateUrl: './profile-picture.component.html',
  styleUrls: ['./profile-picture.component.scss']
})
export class ProfilePictureComponent implements OnInit {
  currentUser: User;
  fileData: File = null;
  previewUrl:any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  constructor(
    public matDialogRef: MatDialogRef<ProfilePictureComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    private _store: Store,
    private _uploadService: UploadService,
    private _userService: UserService) { 

      this._store.select(UserState.profile).subscribe( user => {
        this.currentUser = user;
      });
    }

  ngOnInit(): void {
    this.previewUrl = this.currentUser.userProfile.photo;
    this.matDialogRef.beforeClosed().subscribe(() => {
      this._store.dispatch(new SetProfile(this.currentUser, ActionStateEnum.UPDATE));
    });
  }
  
  fileProgress(fileInput: any) {
        this.fileData = <File>fileInput.target.files[0];
        console.log(this.fileData);
        this.preview();
  }
  
  preview() {
      // Show preview 
      var mimeType = this.fileData.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
  
      var reader = new FileReader();      
      reader.readAsDataURL(this.fileData); 
      reader.onload = (_event) => { 
        this.previewUrl = reader.result; 
      }
  }

  updatePhotoData(){
    this._userService.updateProfile(this.currentUser).subscribe((result) => {
      //console.log(result);
      this.matDialogRef.close();
    });
  }
 
  uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    file.inProgress = true;
    this._uploadService.upload(`/users/${get('id')(this.currentUser) || moment.now() }/upload-images`, formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.data.name} upload failed.`);
      })
    ).subscribe((event: any) => {
      if (typeof (event) === 'object') {
        this.uploadedFilePath = event.body.filePath;
        console.log(event.body.filePath);
        //update data
        this.currentUser = {
          ...this.currentUser,
          userProfile: {
            ...this.currentUser.userProfile,
            photo: this.uploadedFilePath
          }
        }
        if(this.uploadedFilePath){
          this.updatePhotoData();
        }
      }
    });
  }

  onSubmit() {
    //console.log('onSubmit');
    if(this.fileData){
      const file = {data: this.fileData, inProgress: false, progress: 0}
      this.uploadFile(file);
    }
    else{
      this.matDialogRef.close();
    }
  }
}
