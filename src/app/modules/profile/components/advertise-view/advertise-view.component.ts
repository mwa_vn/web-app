import { Component, OnInit } from '@angular/core';
import { advertiseService } from 'modules/advertise/advertise.service';
import { Advertise } from '@domain/models/advertise.model';

@Component({
  selector: 'app-advertise-view',
  templateUrl: './advertise-view.component.html',
  styleUrls: ['./advertise-view.component.css']
})
export class AdvertiseViewComponent implements OnInit {
  
  adver:Advertise[]
  constructor(private _adverservice: advertiseService) { 
  
  }

  ngOnInit(): void {

    this._adverservice.paging(undefined,0,3,"createdOn=desc","homepage").subscribe(data=>{
      this.adver= data.body.result;
     // this._adverservice.pagingInterval(undefined,0,3,"createdOn=desc","homepage").subscribe(httpObservable=>{   
     //   httpObservable.subscribe(data=>{
     //   this.adver= data.body.result;
     //  })
     // }, err=>{
      //  console.log("can not get advertisement");
     // })

    },err=>{
      console.log("can not get advertisement");
    })

  
  }

}
