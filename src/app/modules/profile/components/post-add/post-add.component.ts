import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import Post from "@domain/models/post.model";
import {CurrentWorkingPostState} from "@domain/store/state/current-working-post.state";
import {SetPost} from "@domain/store/actions/post.action";
import {ActionStateEnum} from "@domain/store/enums/action-state.enum";
import {UploadService} from '@domain/services/upload.service';
import {catchError, map} from 'rxjs/operators';
import {HttpErrorResponse, HttpEventType} from '@angular/common/http';
import {of} from 'rxjs';
import {UserState} from "@domain/store/state/user.state";
import {User} from "@domain/models";
import moment from 'moment';
import any from 'lodash/fp/any';
import get from 'lodash/fp/get';
import isEmpty from 'lodash/fp/isEmpty';

@Component({
  selector: 'app-post-add',
  templateUrl: './post-add.component.html',
  styleUrls: ['./post-add.component.scss']
})
export class PostAddComponent implements OnInit {

  postForm: FormGroup;
  currentUser: User;
  post: Post;
  @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;
  files = [];
  filesPathSaved = [];

  constructor(public matDialogRef: MatDialogRef<PostAddComponent>,
              @Inject(MAT_DIALOG_DATA) public _data: any,
              private _store: Store,
              private _uploadService: UploadService
  ) {
    this.postForm = this.createPostForm();
    this._store.select(UserState.profile).subscribe( user => {
      this.currentUser = user;
    });
  }

  createPostForm(): FormGroup {
    return new FormGroup({
      message: new FormControl(''),
      isNotified: new FormControl(false),
    });
  }

  ngOnInit(): void {
    this._store.select(CurrentWorkingPostState.post).subscribe((post: Post) => {
      this.post = post;
      if (post) {
        this.postForm.get('message').setValue(post.message);
      }
    });
    this.matDialogRef.beforeClosed().subscribe(() => {
      this._store.dispatch(new SetPost(this.createPostData(), ActionStateEnum.SAVE));
    });
  }

  createPostData(): Post {
    const posted: Post = {
      message: this.postForm.get('message').value,
      user: this.currentUser,
      images:this.filesPathSaved,
      unhealthy: false,
      isNotified: this.postForm.get('isNotified').value,
      likes: [],
      comments: [],
    };
    return posted;
  }

  uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    file.inProgress = true;
    file.status = '';
    const fileSize = get('data.size')(file);
    if (fileSize) {
      if ((fileSize / (1000 * 1000)) > 1) {
        file.inProgress = false;
        file.status = 'too large size';
        return;
      }
    }
    this._uploadService.upload(`/posts/${moment.now()}/upload-images`, formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        file.status = `${file.data.name} upload failed.`;
        return of(null);
      })
    ).subscribe((event: any) => {
      if (!isEmpty(get('body.filePath')(event))) {
        this.filesPathSaved.push(get('body.filePath')(event));
      } else {
        const error = get('body.error')(event);
        if (!isEmpty(error)) {
          file.status = error;
        }
      }
      file.inProgress = false;
    });
  }

  onClick() {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({data: file, inProgress: false, progress: 0});
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  closeDialog() {
    if (any(f => get('inProgress')(f))(this.files)) return;
    this.matDialogRef.close();
  }
  savePost() {
    if (any(f => get('inProgress')(f))(this.files)) return;
    this.matDialogRef.close(['post', this.postForm]);
  }
  deletePost() {
    if (any(f => get('inProgress')(f))(this.files)) return;
    this.matDialogRef.close(['delete', this.postForm]);
  }

  private uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }
}
