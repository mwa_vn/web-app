import {Component, ViewEncapsulation, OnInit} from '@angular/core';

import {fuseAnimations} from '@fuse/animations';
import {User} from "@domain/models";
import {Store} from "@ngxs/store";
import {UserState} from "@domain/store/state/user.state";
import {MatDialog} from "@angular/material/dialog";
import {EditProfileComponent} from "./components/edit-profile/edit-profile.component";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "@domain/services/user.service";
import {ProfilePictureComponent} from "./components/profile-picture/profile-picture.component";
import {SocialService} from "@domain/services/social.service";

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ProfileComponent implements OnInit{
  profileUser: User;
  loggedUser: User;
  dialogRef: any;
  isHome = false;
  profileUserId: string;
  isFollowed = false;

  constructor(private _store: Store,
              public _matDialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              private _socialService: SocialService,
              private userService: UserService) { }

  ngOnInit() {
    this.isHome = this.router.url === '/home';
    this.route.params.subscribe(
      params => {
        this.profileUserId = params.userId;
        // To check other user go to see this profile
        if (this.profileUserId) {
          this.userService.getProfile(this.profileUserId).subscribe(user => {
            if (!user.userProfile.photo) {
              user.userProfile.photo = "assets/images/avatars/profile.jpg";
            }
            this.profileUser = user;
          });
        }

        this._store.select(UserState.profile).subscribe(user => {
          if (user) {
            this.loggedUser = user;
            if(!this.profileUserId) {
              this.profileUser = user;
            }
            this.isFollowed = (this.loggedUser.following.find( user => user === this.profileUserId) !== undefined)
          }
        });
      }
    );
  }

  openPostDialog(): void {
    this.dialogRef = this._matDialog.open(EditProfileComponent, {
      panelClass: 'app-post-add',
      autoFocus: true,
      data: {
        userProfile: this.profileUser.userProfile
      }
    });
    this.dialogRef.afterClosed()
      .subscribe(response => {
        if (!response) {
          return;
        }
        const actionType: string = response[0];
        switch (actionType) {
          case 'post':
            //this.postComment(this.currentPost);
            console.log('create Post');
            break;
        }
      });
  }

  follow() {
    this._socialService.updateFollow(this.profileUserId, !this.isFollowed).subscribe(() => {
      this.isFollowed = !this.isFollowed;
    });
  }
  openProfileDialog(): void {
    this.dialogRef = this._matDialog.open(EditProfileComponent, {
      panelClass: 'app-post-add',
      autoFocus: true,
      //minWidth: 500,
      // data: {
      //   userProfile: this.profileUser.userProfile
      // }
    });
  }

  editprofilePhoto(): void {
    this.dialogRef = this._matDialog.open(ProfilePictureComponent, {
      panelClass: 'app-post-add',
      autoFocus: true,

    });
  }
}
