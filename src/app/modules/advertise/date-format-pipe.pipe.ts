import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateFormatPipe'
})
export class DateFormatPipePipe implements PipeTransform {

  transform(value: Date|moment.Moment, ...args: unknown[]): unknown {
    if(!value)
      return "NaN";
    else
     return moment(value).fromNow();
  }

}
