import { Component, OnInit, OnDestroy } from '@angular/core';
import { advertiseService } from '../advertise.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Advertise } from '@domain/models/advertise.model';
import { environment } from 'environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-detailad',
  templateUrl: './detailad.component.html',
  styleUrls:['./detailad.component.css']
})
export class DetailadComponent implements OnInit, OnDestroy {
  id: string;
  ad: Advertise;
  urlAPI = environment.apiUrl;
  urlImage: any = null;
  urlImageS3:string='';
  adform: FormGroup;
  file: any;
  private subscription:Subscription;
  constructor(private advertiseService: advertiseService, private activatedroute: ActivatedRoute
    , private router: Router, private formbuilder: FormBuilder, private _snackBar: MatSnackBar) {
    //for "Create new One?" button which can't refresh data without this code.
    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    }

  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.adform = this.formbuilder.group({
      _id: [''],
      name: ['',[Validators.required,Validators.minLength(40), Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.minLength(50), Validators.maxLength(108)]],
      linkUrl: ['', [Validators.required,Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/)]],
      imageUrl:[''],
      createdOn:[],
      operate:[''],
      age:[],
      logic:[''],
      zipcode:[''],

    });
   
    this.subscription = this.activatedroute.params.subscribe(param => {
      this.id = param['id'];
      if (this.id) //edit
       { 
        this.advertiseService.getById(this.id).then((data) => {
          this.urlImageS3=data.result.imageUrl;
          // this.adform.controls.operate.patchValue("$gt"); // set value of control 
          // this.adform.controls.logic.patchValue("$and"); 
          this.adform.setValue(data.result);        
          this.addValidateOnChange();          
        })
      }
      else 
        this.addValidateOnChange(); 
    });


  }

  private addValidateOnChange() {
    this.adform.controls["operate"].valueChanges.subscribe(this.addValidate());
    this.adform.controls["logic"].valueChanges.subscribe(this.addValidate());
    this.adform.controls["zipcode"].valueChanges.subscribe(this.addValidate());
    this.adform.controls["age"].valueChanges.subscribe(this.addValidate());
  }

  onSubmit() {

    if (!this.adform.value._id) {
      delete this.adform.value._id;
      this.advertiseService.add(this.adform.value)
        .subscribe(data => {
          this.susscess();
          console.log(data);
          this.adform.setValue(data.result);
          this.router.navigate(['', 'advertise',data.result._id]);
        },err=>{
          this.error();
        })
    }
    else //edit
    {

      this.advertiseService.update(this.adform.value._id, this.adform.value)
        .then(data => {
          this.susscess();
          this.router.navigate(['', 'advertise']);
        }).catch(err=>{
          this.error();
        })
    }
  }

  inputFileOnChange(event) {
    let files = event.target.files;
    if (files.length === 0)
      return;
    this.file = files[0];
    var name = this.file.name;

    if (!name.match(/\.(jpg|jpeg|png)$/)) {
      return;
    }  

    var reader = new FileReader();
    reader.readAsDataURL(this.file);
    reader.onload = (_event) => {
      this.urlImage = reader.result;
    }
  }

  upload(): void {
    console.log(this.file);
     this.advertiseService.postFile(this.id,this.file).subscribe(()=>{
       this.susscess();
       this.router.navigate(['', 'advertise',this.id]);
     },err=>{
      this.error();
     })
  }
  reset(event): void {
    this.adform.controls["logic"].reset('');   
    this.adform.controls["zipcode"].reset('');   
    this.adform.controls["operate"].reset('');  
    this.adform.controls["age"].reset('');
    this.resetValidate();
  }
  private resetValidate(){
  
    this.adform.controls["logic"].setErrors(null);
    this.adform.controls["zipcode"].setErrors(null);  
    this.adform.controls["operate"].setErrors(null);   
    this.adform.controls["age"].setErrors(null);
  }

  private susscess() {
    this._snackBar.open("Sucess", 'OK', {
      verticalPosition: 'top', horizontalPosition: 'center',
      duration: 3000, panelClass: 'ErrorSnackbar'
    });
  }

  private error() {
    this._snackBar.open("Error", 'OK', {
      verticalPosition: 'top', horizontalPosition: 'right',
      duration: 3000, panelClass: 'ErrorSnackbar'
    });
  }


  private addValidate(): (value: any) => void {
    return selected => {
      
        this.adform.controls["operate"].setValidators(Validators.required);
        this.adform.controls["operate"].updateValueAndValidity();
        this.adform.controls["age"].setValidators(Validators.required);
        this.adform.controls["age"].updateValueAndValidity();
        this.adform.controls["logic"].setValidators([Validators.required]);
        this.adform.controls["logic"].updateValueAndValidity();
        this.adform.controls["zipcode"].setValidators([Validators.required,Validators.pattern(/^[0-9]{5}(?:-[0-9]{4})?$/)]);
        this.adform.controls["zipcode"].updateValueAndValidity();
     
    };
  }
   

}

