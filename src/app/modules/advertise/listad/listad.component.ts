
import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { advertiseService } from '../advertise.service';
import { Advertise } from '@domain/models/advertise.model';
import { paginate, DisplayRow } from './paginate';

import { environment } from 'environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({ 
  selector: 'app-listad',
  templateUrl: './listad.component.html',
  styleUrls: ['./listad.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ListadComponent implements OnInit {
  displayRow: any[] = DisplayRow();
  totalPage: number = 0;
  totalRow: number = 0;
  advertise: Advertise[];
  loadComplete = false;
  page = new paginate(10, 0, 5, 10);
  paging = this.page.getPage();
  searchString: string;
  urlAPI = environment.apiUrl;

  constructor(private advertiseService: advertiseService, private _snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    this.loadData();
  }

  private loadData() {

    if (!this.searchString) this.searchString = undefined;
    this.loadComplete = false;
    let page = this.paging.currentPage - 1;

    this.advertiseService.paging(this.searchString, page, this.paging.pageSize, 'createdOn=desc')
      .subscribe((res) => {
        this.advertise = res.body.result;
        this.totalRow = Number.parseInt(res.headers.get("X-Row-Header"));
        this.totalPage = Number.parseInt(res.headers.get("X-Paging-Header"));

        let page = new paginate(this.totalPage, this.paging.currentPage, this.paging.pageSize, 2);
        this.paging = page.getPage();
        this.loadComplete = true;
      });
  }

  next(event: any): void {
    
    let page = new paginate(this.paging.totalPages, this.paging.currentPage += 1, this.paging.pageSize, 2);
    this.paging = page.getPage();
    console.log(this.paging);

    this.loadData();
  }

  previous(event: any): void {
    let page = new paginate(this.paging.totalPages, this.paging.currentPage -= 1, this.paging.pageSize, 2);
    this.paging = page.getPage();
    this.loadData();
  }
  search(input: any): void {
    this.searchString = input.value;
    let page = new paginate(this.paging.totalPages, 0, this.paging.pageSize, 2);
    this.paging = page.getPage();
    this.loadData();
  }
  selectPage(current): void {
    this.paging.currentPage = current;
    let page = new paginate(this.paging.totalPages, this.paging.currentPage, this.paging.pageSize, 2);
    this.paging = page.getPage();

    this.loadData();
  }
  edit(id) {

  }
  delete(id) {
    this.advertiseService.delete(id).then(() => {
      this.susscess();
      this.loadData();
    }).catch(err => {
      this.error();
    })

  }

  onChangeDisplay(event) {
    this.paging.pageSize = +event.target.value;
    let page = new paginate(this.paging.totalPages, this.paging.currentPage, this.paging.pageSize, 2);
    this.paging = page.getPage();
    this.loadData();
  }
  private susscess() {
    this._snackBar.open("Sucess", 'OK', {
      verticalPosition: 'top', horizontalPosition: 'center',
      duration: 3000, panelClass: 'ErrorSnackbar'
    });
  }

  private error() {
    this._snackBar.open("Error", 'OK', {
      verticalPosition: 'top', horizontalPosition: 'right',
      duration: 3000, panelClass: 'ErrorSnackbar'
    });
  }

}

