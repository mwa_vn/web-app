
export class paginate {
    totalPages: number;
    currentPage: number = 1;
    pageSize: number = 10;
    maxPages: number = 10

    constructor(
        totalPages: number,
        currentPage: number = 1,
        pageSize: number = 10,
        maxPages: number = 10
    ) {
        this.totalPages = totalPages;
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.maxPages = maxPages;
    }
    // calculate total pages
    // let totalPages = Math.ceil(totalItems / pageSize);
    getPage() {
        // ensure current page isn't out of range
        if (this.currentPage < 1) {
            this.currentPage = 1;
        } else if (this.currentPage > this.totalPages) {
            this.currentPage = this.totalPages;
        }

        let startPage: number, endPage: number;
        if (this.totalPages <= this.maxPages) {
            // total pages less than max so show all pages
            startPage = 1;
            endPage = this.totalPages;
        } else {
            // total pages more than max so calculate start and end pages
            let maxPagesBeforeCurrentPage = Math.floor(this.maxPages / 2);
            let maxPagesAfterCurrentPage = Math.ceil(this.maxPages / 2) - 1;
            if (this.currentPage <= maxPagesBeforeCurrentPage) {
                // current page near the start
                startPage = 1;
                endPage =this.maxPages;
            } else if (this.currentPage + maxPagesAfterCurrentPage >= this.totalPages) {
                // current page near the end
                startPage = this.totalPages - this.maxPages + 1;
                endPage = this.totalPages;
            } else {
                // current page somewhere in the middle
                startPage = this.currentPage - maxPagesBeforeCurrentPage;
                endPage = this.currentPage + maxPagesAfterCurrentPage;
            }
        }

        // calculate start and end item indexes
        let startIndex = (this.currentPage - 1) * this.pageSize;
        let endIndex = Math.min(startIndex + this.pageSize - 1, this.totalPages - 1);

        // create an array of pages to ng-repeat in the pager control
        let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

        // return object with all pager properties required by the view
        return {
            totalPages: this.totalPages,
            currentPage: this.currentPage,
            pageSize: this.pageSize,           
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
            
        };
    }
}

export function DisplayRow(){
    return [{
        id:5,
        name: "5 rows"
    },{
        id:10,
        name: "10 rows"
    },{
        id:20,
        name: "20 rows"
    },{
        id:30,
        name: "30 rows"
    },{
        id:50,
        name: "50 rows"
    }]
}

