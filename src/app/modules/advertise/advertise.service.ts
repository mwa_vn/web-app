 import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable, interval } from 'rxjs';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class advertiseService implements Resolve<any> {
  ads: any[];
  onAdChanged: BehaviorSubject<any>;

  constructor(  
    private http: HttpClient
  ) {
    this.onAdChanged = new BehaviorSubject({});
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.get()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

   get(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${environment.apiUrl}/advertise`).subscribe(
        (response: any) => {
          this.ads = response;
          this.onAdChanged.next(this.ads);
          resolve(response);
        },
        reject
      );
    });
  }
 
  getById(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${environment.apiUrl}/advertise/${id}`).subscribe(
        (response: any) => {
          this.ads = response;
          this.onAdChanged.next(this.ads);
          resolve(response);
        },
        reject
      );
    });
  }

  paging(search, page, pagesize, sort, type?:string): Observable<any> {
     return this.http.get(`${environment.apiUrl}/advertise/page/${page}/${pagesize}/${sort}?type=${type}&search=${search}`,{ observe: 'response' }); // { observe: 'response' } return header in httpclient
  }

  pagingInterval(search, page, pagesize, sort, type?:string): Observable<any> {   
     return interval(5000).pipe(
       map(x=>{      
        return this.http.get(`${environment.apiUrl}/advertise/page/${page}/${pagesize}/${sort}?type=${type}&search=${search}`,{ observe: 'response' }); 
       })
     )   
  }

  add(ad:any):Observable<any> {
    return this.http.post(`${environment.apiUrl}/advertise`, ad);
  }
  update(id, ad) {
    return new Promise((resolve, reject) => {
      this.http.put(`${environment.apiUrl}/advertise/${id}`, ad ).subscribe(
        (response: any) => resolve(response),
        reject
      );
    });
  }
  delete(id) {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl}/advertise/${id}`)
        .subscribe(
          (response: any) => resolve(response),
          reject
        );
    })
  }

  postFile(id:string,file:File):Observable<any>{
    const formData:FormData=new FormData();
    formData.append("file", file, file.name);
   // formData.append("id",id);
    return this.http.post(`${environment.apiUrl}/advertise/upload?id=${id}`, formData);
  }
}
