import {NgModule} from '@angular/core';

import { RouterModule, ActivatedRoute } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule, FuseConfirmDialogModule } from '@fuse/components';
import { MatDialogModule } from '@angular/material/dialog';
import { advertiseService } from './advertise.service';
import { DetailadComponent } from './detailad/detailad.component';
import { ListadComponent } from './listad/listad.component';
import { DateFormatPipePipe } from './date-format-pipe.pipe';
import { LoadingImageDirective } from './loading-image.directive';
import { ReactiveFormsModule } from '@angular/forms';

const routes = [
  {
    path: '',
    component: ListadComponent
    // resolve  : {
    //   data: advertiseService
    // }
  },
  {
    path: ':id',
    component: DetailadComponent,
    resolve  : {
      data: advertiseService
    }
  }
];
@NgModule({
  declarations: [
    ListadComponent,
    DetailadComponent,
    DateFormatPipePipe,
    LoadingImageDirective
  ],
  imports: [
    RouterModule.forChild(routes), 
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatDialogModule,

  
    FuseSharedModule,
    FuseWidgetModule,
    FuseConfirmDialogModule
  ],
  providers: [
    advertiseService
  ]
})
export class AdvertiseModule {
}
