import { Directive, Renderer2, ElementRef, Input, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appLoadingImage]'
})
export class LoadingImageDirective {

  @Input() imageLoader:string;

  @HostListener('load') loadImage(){
    setTimeout(()=>{
      this.src=this.imageLoader;
      console.log(this.src);
    },1000)
  
  };
  @HostBinding('attr.src') src="../../../assets/icons/flags/noimageLoading.gif";
  constructor(private el:ElementRef) {  
  }
  ngOnInit() {  
  }

}
