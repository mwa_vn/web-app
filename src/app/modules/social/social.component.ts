import { Component, OnInit, OnDestroy, ElementRef, ViewChild, NgZone } from '@angular/core';
import { SocialService } from '@domain/services/social.service';
import { User } from '@domain/models';
import {Store} from '@ngxs/store';
import {UserState} from '@domain/store/state/user.state';
import { Router } from '@angular/router';
import last from 'lodash/fp/last';
import get from 'lodash/fp/get';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit, OnDestroy {

  searchTerm = '';
  currentCategory = 'all';
  currentUser: User;
  users = [];
  @ViewChild('peopleAnchor', { read: ElementRef }) peopleAnchor: ElementRef;
  loadingData = false;

  constructor(
    private socialService: SocialService,
    private store: Store,
    private router: Router,
    private ngZone: NgZone
  ) {
    this.store.select(UserState.profile).subscribe( user => {
      this.currentUser = user;
    });
  }

  ngOnInit(): void {
    this.fetchData().then(() => {
      this.ngZone.runOutsideAngular(() => {
        window.addEventListener('scroll', this.scroll, true);
      });
    });
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (ev) => {
    if ((ev.srcElement.scrollTop + ev.srcElement.offsetHeight) >= this.peopleAnchor.nativeElement.offsetTop) {
      this.ngZone.run(() => this.fetchData());
      ev.stopPropagation();
      ev.preventDefault();
    }
  }

  doSearch() {
    this.users.length = 0;
    this.fetchData();
  }

  changeCategory() {
    this.users.length = 0;
    this.fetchData();
  }

  doFollow(user) {
    this.socialService
    .updateFollow(user.id, !user.followed)
    .subscribe(
      res => {
        this.users.length = 0;
        this.fetchData();
      },
      err => console.log(err)
    );
  }

  fetchData() {
    if (this.loadingData) { return; }
    const lastId = get('id')(last(this.users)) || '';
    this.loadingData = true;
    return new Promise((resolve, reject) => {
      // console.log(this.currentCategory);
      this.socialService.fetchPeople(this.searchTerm, this.currentCategory, lastId).subscribe(
        result => {
          // console.log(result);
          this.users.push(...result);
          this.loadingData = false;
          resolve();
        },
        err => reject(err)
      );
    });
  }
}
