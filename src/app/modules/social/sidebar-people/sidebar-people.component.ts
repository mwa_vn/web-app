import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { SocialService } from '@domain/services/social.service';
import { Store } from '@ngxs/store';
import { UserState } from '@domain/store/state/user.state';
import { User } from '@domain/models';

@Component({
  selector: 'app-sidebar-people',
  templateUrl: './sidebar-people.component.html',
  styleUrls: ['./sidebar-people.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SidebarPeopleComponent implements OnInit {

  @Input()
  title = 'People';
  @Input()
  topPeople = false;
  currentUser: User;

  users = [];

  constructor(private socialService: SocialService, private store: Store) {
    this.store.select(UserState.profile).subscribe( user => {
      this.currentUser = user;
    });
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.socialService.fetchTop10People().subscribe(
      res => {
        this.users = res;
      },
      err => console.log(err)
    );
  }

}
