importScripts('https://www.gstatic.com/firebasejs/7.13.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.13.1/firebase-messaging.js');

firebase.initializeApp({
  apiKey: "AIzaSyA7oVwVhshtukO4x2FZbu5HqlMDply2a64",
  authDomain: "mwa-vn.firebaseapp.com",
  databaseURL: "https://mwa-vn.firebaseio.com",
  projectId: "mwa-vn",
  storageBucket: "mwa-vn.appspot.com",
  messagingSenderId: "778470794747",
  appId: "1:778470794747:web:cc1ea70eeb0535ecf2975e",
  measurementId: "G-7YFP0RSRTN"
});

const messaging = firebase.messaging();