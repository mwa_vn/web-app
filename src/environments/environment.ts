// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:3001/api',
  firebase: {
    apiKey: "AIzaSyA7oVwVhshtukO4x2FZbu5HqlMDply2a64",
    authDomain: "mwa-vn.firebaseapp.com",
    databaseURL: "https://mwa-vn.firebaseio.com",
    projectId: "mwa-vn",
    storageBucket: "mwa-vn.appspot.com",
    messagingSenderId: "778470794747",
    appId: "1:778470794747:web:cc1ea70eeb0535ecf2975e",
    measurementId: "G-7YFP0RSRTN"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error';  // Included with Angular CLI.
