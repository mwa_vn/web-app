import { Pipe, PipeTransform } from '@angular/core';
import join from 'lodash/fp/join';

@Pipe({name: 'joinStrings'})
export class JoinStringsPipe implements PipeTransform
{
    transform(value: any): any
    {
      return join(', ')(value);
    }
}
